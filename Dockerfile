FROM registry.gitlab.com/audiochills/keycloak-containers:14.0.0-audiochills1

FROM jboss/keycloak:15.0.2

# ADD ./modules/keycloak-kafka-1.0.0-jar-with-dependencies.jar /standalone/deployments/
# ADD ./modules/kafka-module.cli /opt/jboss/startup-scripts/
# ENV JAVA_TOOL_OPTIONS "${JAVA_TOOL_OPTIONS} -Dkafka.enabled=${KAFKA_ENABLED} -Dkafka.topicEvents=${KAFKA_TOPIC_EVENTS} -Dkafka.clientId=${KAFKA_CLIENT_ID} -Dkafka.bootstrapServers=${KAFKA_BOOTSTRAP_SERVERS} -Dkafka.events=${KAFKA_EVENTS}"

COPY ./themes/audiochills /opt/jboss/keycloak/themes/audiochills

# ADD ./realm-export.json /init/
# ENV JAVA_TOOL_OPTIONS "-Dkeycloak.import=/init/realm-export.json"

EXPOSE 5000

CMD ["-Djboss.http.port=5000"]