<#import "template.ftl" as layout>
<@layout.mainLayout active='account' bodyClass='user'; section>

    <div class="row my-3">
        <h2>${msg("editAccountHtmlTitle")}</h2>
    </div>

    <form action="${url.accountUrl}" class="form-horizontal" method="post">

        <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">

        <input type="hidden" class="form-control" id="username" name="username" <#if !realm.editUsernameAllowed>disabled="disabled"</#if> value="${(account.username!'')}"/>

        <div class="form-group ${messagesPerField.printIfExists('email','has-error')}">
            <div class="col-sm-2 col-md-4">
                <label for="email" class="control-label">${msg("email")}</label> <span class="required">(required)</span>
            </div>

            <div class="col-sm-12 col-md-12">
                <input type="email" class="form-control" id="email" name="email" required autofocus value="${(account.email!'')}"/>
            </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('firstName','has-error')}">
        
            <div class="col-sm-2 col-md-6">
                <label for="firstName" class="control-label">${msg("firstName")}</label> <span class="required">(required)</span>
            </div>

            <div class="col-sm-12 col-md-12">
                <input type="text" class="form-control" id="firstName" name="firstName" required value="${(account.firstName!'')}"/>
            </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('lastName','has-error')}">
            <div class="col-sm-2 col-md-6">
                <label for="lastName" class="control-label">${msg("lastName")}</label> <span class="required">(required)</span>
            </div>

            <div class="col-sm-12 col-md-12">
                <input type="text" class="form-control" id="lastName" name="lastName" required value="${(account.lastName!'')}"/>
            </div>
        </div>

        <div class="form-group">
            <div id="kc-form-buttons" class="col-md-offset-2 col-md-10 submit">
                <div class="">
                    <#if url.referrerURI??><a href="${url.referrerURI}">${kcSanitize(msg("backToApplication")?no_esc)}</a></#if>
                    <button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Save">${msg("doSave")}</button>
                    <button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Cancel">${msg("doCancel")}</button>
                </div>
            </div>
        </div>
    </form>

</@layout.mainLayout>
