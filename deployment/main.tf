terraform {
  backend "http" {}
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.0.2"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.0.2"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.0.1"
    }
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.11.0"
    }
  }
}

resource "kubernetes_namespace" "namespace" {
  metadata {
    name   = var.kube_namespace
    labels = merge(var.kube_annotations, { name = var.kube_namespace })
  }
}

resource "kubernetes_secret" "image_pull_secrets" {
  metadata {
    name      = "gitlab-registry"
    namespace = kubernetes_namespace.namespace.metadata.0.name
  }
  data = {
    ".dockerconfigjson" = templatefile("${path.module}/dockerconfig.json", {
      docker_registry_name     = var.docker_registry_name
      docker_registry_username = var.docker_registry_username
      docker_registry_password = var.docker_registry_password
    })
  }
  type = "kubernetes.io/dockerconfigjson"
}

provider "docker" {
  registry_auth {
    address = "registry.gitlab.com"
  }
}

resource "docker_registry_image" "keycloak" {
  name = "${var.docker_registry_name}/${var.docker_registry_image_repository}/keycloak:${var.docker_registry_image_tag}"
  build {
    context  = ".."
    build_id = uuid()
  }
}

resource "random_password" "keycloak_admin_password" {
  length  = 16
  special = false
}

resource "helm_release" "keycloak" {
  name       = "keycloak"
  repository = "https://codecentric.github.io/helm-charts"
  chart      = "keycloak"
  version    = "9.5.0"
  namespace  = kubernetes_namespace.namespace.metadata.0.name
  values = [
    templatefile("${path.module}/keycloak.values.yaml", {
      keycloak_domain         = var.keycloak_domain,
      keycloak_admin_user     = "admin",
      keycloak_admin_password = random_password.keycloak_admin_password.result,
      kube_annotations        = var.kube_annotations
    })
  ]

  set {
    name  = "image.repository"
    value = "${var.docker_registry_name}/${var.docker_registry_image_repository}/keycloak@sha256"
  }
  set {
    name  = "image.tag"
    value = trimprefix(docker_registry_image.keycloak.sha256_digest, "sha256:")
  }
  set {
    name  = "image.pullPolicy"
    value = "Always"
  }
  set {
    name  = "imagePullSecrets[0].name"
    value = kubernetes_secret.image_pull_secrets.metadata.0.name
  }
}

output "keycloak_url" {
  value = "https://${var.keycloak_domain}"
}
