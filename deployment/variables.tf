variable "docker_registry_name" {
  type = string
}
variable "docker_registry_username" {
  type = string
}
variable "docker_registry_password" {
  type = string
}
variable "docker_registry_image_tag" {
  type = string
}
variable "docker_registry_image_repository" {
  type = string
}
variable "keycloak_domain" {
  description = "The domain used for this keycloak server."
}
variable "kube_annotations" {
  type    = map(string)
  default = {}
}
variable "kube_namespace" {
  type    = string
  default = "default"
}
